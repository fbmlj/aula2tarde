class CreateForms < ActiveRecord::Migration[5.2]
  def change
    create_table :forms do |t|
      t.string :email
      t.string :phone
      t.string :issue
      t.string :message
      t.string :name

      t.timestamps
    end
  end
end
