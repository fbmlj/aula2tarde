Rails.application.routes.draw do
  resources :posts
  get 'form/new',to: 'form#new',as:  :new_form
  get 'form/', to: 'form#index', as: :forms
  get 'form/:id/edit', to: 'form#edit', as: :edit_form
  post 'form', to: 'form#create', as: :create_form
  get 'form/:id', to: 'form#show', as: :form
  put 'form/:id', to: 'form#update', as: :form_update
  delete 'form/:id', to: 'form#destroy', as: :form_delete
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
