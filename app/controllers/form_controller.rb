class FormController < ApplicationController
  before_action :form_find, only: [:show,:destroy,:edit,:update]
  def index
    @forms = Form.all
  end
  
  def show
  end

  def edit
  end
  
  def update
    @form.update(email: params[:form][:email], name: params[:form][:name], phone: params[:form][:phone], issue: params[:form][:issue], message: params[:form][:message])
    if @form.present?
      redirect_to form_path(id: @form.id)
    end
  
  end
  
  def new
    @form = Form.new
  end
  def create
    @form = Form.create(email: params[:form][:email], name: params[:form][:name], phone: params[:form][:phone], issue: params[:form][:issue], message: params[:form][:message])
    if @form.present?
      redirect_to form_path(id: @form.id)
    end
  end
  
  def destroy
    @form.destroy
    redirect_to forms_path
  end

  private

    def form_find
      @form = Form.find(params[:id])
    end
end
